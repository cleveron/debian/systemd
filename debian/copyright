Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: systemd
Upstream-Contact: systemd-devel@lists.freedesktop.org
Source: https://www.freedesktop.org/wiki/Software/systemd/

Files: *
Copyright: 2008-2023 systemd contributors
License: LGPL-2.1-or-later

Files:
 src/basic/siphash24.h
 src/basic/siphash24.c
Copyright:
 2012 Daniel J. Bernstein <djb@cr.yp.to>
 2012 Jean-Philippe Aumasson <jeanphilippe.aumasson@gmail.com>
License: CC0-1.0

Files:
 src/shared/linux/*
 src/basic/linux/*
Copyright: 2004-2023 systemd contributors
License: GPL-2.0-only with Linux-syscall-note exception

Files: src/basic/sparse-endian.h
Copyright: 2012 Josh Triplett <josh@joshtriplett.org>
License: Expat

Files:
 src/libsystemd/sd-journal/lookup3.c
 src/libsystemd/sd-journal/lookup3.h
Copyright: none
License: public-domain
 You can use this free for any purpose. It's in the public domain. It has no
 warranty.

Files:
 src/udev/ata_id/ata_id.c
 src/udev/cdrom_id/cdrom_id.c
 src/udev/mtd_probe/mtd_probe.c
 src/udev/mtd_probe/mtd_probe.h
 src/udev/mtd_probe/probe_smartmedia.c
 src/udev/scsi_id/scsi.h
 src/udev/scsi_id/scsi_id.c
 src/udev/scsi_id/scsi_id.h
 src/udev/scsi_id/scsi_serial.c
 src/udev/udevadm.c
 src/udev/udevadm-control.c
 src/udev/udevadm.h
 src/udev/udevadm-info.c
 src/udev/udevadm-monitor.c
 src/udev/udevadm-settle.c
 src/udev/udevadm-test-builtin.c
 src/udev/udevadm-test.c
 src/udev/udevadm-trigger.c
 src/udev/udevadm-util.c
 src/udev/udevadm-util.h
 src/udev/udev-builtin-blkid.c
 src/udev/udev-builtin.h
 src/udev/udev-builtin-input_id.c
 src/udev/udev-builtin-kmod.c
 src/udev/udev-builtin-path_id.c
 src/udev/udev-builtin-uaccess.c
 src/udev/udev-builtin-usb_id.c
 src/udev/udev-ctrl.h
 src/udev/udevd.c
 src/udev/udev-event.c
 src/udev/udev-event.h
 src/udev/udev-node.c
 src/udev/udev-node.h
 src/udev/udev-rules.c
 src/udev/udev-rules.h
 src/udev/udev-watch.c
 src/udev/udev-watch.h
 src/udev/v4l_id/v4l_id.c
Copyright: 2003-2023 systemd contributors
License: GPL-2.0-or-later

Files: debian/*
Copyright:
 2010-2013 Tollef Fog Heen <tfheen@debian.org>
 2013-2018 Michael Biebl <biebl@debian.org>
 2013 Michael Stapelberg <stapelberg@debian.org>
 2020-2024 Clevon AS <info@clevon.com>
License: LGPL-2.1-or-later

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

License: GPL-2.0-only with Linux-syscall-note exception
 NOTE! This copyright does *not* cover user programs that use kernel services
 by normal system calls - this is merely considered normal use of the kernel,
 and does *not* fall under the heading of "derived work". Also note that the
 GPL below is copyrighted by the Free Software Foundation, but the instance of
 code that it refers to (the Linux kernel) is copyrighted by me and others who
 actually wrote it.
 .
 Also note that the only valid version of the GPL as far as the kernel is
 concerned is _this_ particular version of the license (ie v2, not v2.2 or v3.x
 or whatever), unless explicitly otherwise stated.
 .
 Linus Torvalds
 .
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian and systems the full text of the GNU General Public License version
 2 can be found in the file '/usr/share/common-licenses/GPL-2'.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51
 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License version
 2 can be found in '/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1-or-later
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License along
 with this program; if not, write to the Free Software Foundation, Inc., 51
 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in '/usr/share/common-licenses/LGPL-2.1'.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in '/usr/share/common-licenses/CC0-1.0'.
