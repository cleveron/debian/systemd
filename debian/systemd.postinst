#!/usr/bin/sh -e

_systemctl() {
	if [ -z "$DPKG_ROOT" ] && [ -d /run/systemd/system ]; then
		systemctl "$@"
	fi
}

_update_catalog() {
	journalctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} --update-catalog || true
}

_update_binfmt() {
	_systemctl restart systemd-binfmt.service || true
}

if [ "$1" = "triggered" ]; then
	shift
	for trigger in $@; do
		case $trigger in
			/usr/lib/binfmt.d) _update_binfmt;;
			/usr/lib/systemd/catalog) _update_catalog;;
		esac
	done
	exit 0
fi

# Enable things by default on new installs.
if [ -z "$2" ]; then
	systemctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} enable getty@tty1.service || true
	systemctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} enable remote-fs.target || true
	systemctl ${DPKG_ROOT:+--root="$DPKG_ROOT"} enable systemd-pstore.service || true
	mkdir -p "$DPKG_ROOT/var/log/journal"
fi

# Create /etc/machine-id.
systemd-machine-id-setup ${DPKG_ROOT:+--root="$DPKG_ROOT"}

# Initial update.
_update_catalog

# Move the old locale file into /etc. Symlinks will be created by tmpfiles.d later.
if [ -f /etc/default/locale ] && [ ! -L /etc/default/locale ] && [ ! -f /etc/locale.conf ]; then
	mv /etc/default/locale /etc/locale.conf
fi

#DEBHELPER#

if [ -n "$2" ]; then
	_systemctl daemon-reexec || true
	# Re-exec user instances so that running user managers are updated too.
	# SIGRTMIN+25→reexec. Note that this is asynchronous, but we can't use
	# D-Bus as dbus-user-session is not guaranteed to be available.
	_systemctl kill --kill-whom='main' --signal='SIGRTMIN+25' 'user@*.service' || true

	_systemctl try-restart systemd-networkd.service || true
	_systemctl try-restart systemd-journald.service || true
fi
